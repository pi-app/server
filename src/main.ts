import * as functions from 'firebase-functions';
import axios from 'axios';
import cors from 'cors';

const PI_API = 'https://api.minepi.com/v2';
const KEY = 'ojfzuoamryns1vitbof21dnqqt61b4mcrad7mbgbs4m737kct5jdjstx7cznf5ca';

export default {
  approve: functions.https.onRequest((request, response) => {
    cors({ origin: true })(request, response, async () => {
      functions.logger.info(`paymentId: ${request.body}`, { structuredData: true });
      try {
        const x = await axios.post(`${PI_API}/payments/${request.body.paymentId}/approve`, {}, {
          headers: { Authorization: `Key ${KEY}` },
        });
        functions.logger.info(`approve: ${x.status}`, { structuredData: true });
        response.send();
      } catch (error) {
        functions.logger.error(error.message);
        response.sendStatus(500);
      }
    });
  }),

  complete: functions.https.onRequest((request, response) => {
    cors({ origin: true })(request, response, async () => {
      functions.logger.info(`paymentId: ${request.body.paymentId}`, { structuredData: true });
      functions.logger.info(`transactionId: ${request.body.transactionId}`, { structuredData: true });
      try {
        const x = await axios.post(`${PI_API}/payments/${request.body.paymentId}/complete`, {
          txid: request.body.transactionId,
        }, {
          headers: { Authorization: `Key ${KEY}` },
        });
        functions.logger.info(`approve: ${x.status}`, { structuredData: true });
        response.send();
      } catch (error) {
        functions.logger.error(error.message);
        response.sendStatus(500);
      }
    });
  }),
};
